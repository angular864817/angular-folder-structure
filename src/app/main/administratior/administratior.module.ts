import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministratiorRoutingModule } from './administratior-routing.module';
import { TeamMembersComponent } from './team-members/team-members.component';


@NgModule({
  declarations: [
    TeamMembersComponent
  ],
  imports: [
    CommonModule,
    AdministratiorRoutingModule
  ]
})
export class AdministratiorModule { }
