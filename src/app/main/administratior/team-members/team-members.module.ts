import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamMembersRoutingModule } from './team-members-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TeamMembersRoutingModule
  ]
})
export class TeamMembersModule { }
