import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserTypesRoutingModule } from './user-types-routing.module';
import { TypesListComponent } from './types-list/types-list.component';
import { TypesAddComponent } from './types-add/types-add.component';
import { TypesDetailComponent } from './types-detail/types-detail.component';


@NgModule({
  declarations: [
    TypesListComponent,
    TypesAddComponent,
    TypesDetailComponent
  ],
  imports: [
    CommonModule,
    UserTypesRoutingModule
  ]
})
export class UserTypesModule { }
