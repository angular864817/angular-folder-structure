import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesAddComponent } from './types-add.component';

describe('TypesAddComponent', () => {
  let component: TypesAddComponent;
  let fixture: ComponentFixture<TypesAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TypesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
