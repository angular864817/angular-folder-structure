import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MailSettingsComponent } from './mail-settings/mail-settings.component';



@NgModule({
  declarations: [
    MailSettingsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SettingsModule { }
